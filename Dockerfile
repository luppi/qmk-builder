# Going to leverage the pip QMK CLI package, so start with a python base
FROM python:buster

# Setup a directory just for the QMK stuff
WORKDIR /qmk

# Image does not have sudo, but the install scripts need one
# so create a fake sudo to just run the command as-is
RUN echo "\$@" > /usr/local/bin/sudo
RUN chmod +x /usr/local/bin/sudo

# get firmware for ErgodoxEZ/PlanckEZ/Moonlander
# recommended to use this fork over the core QMK firmware
RUN git clone https://github.com/qmk/qmk_firmware.git /qmk

# install the QMK CLI tooling
RUN pip install qmk

# Ensure QMK setup is ready to run smoothly
RUN make git-submodule

# Auto-run the QMK setup with the custom QMK home directory
RUN qmk setup -y -H /qmk

# Entrypoint is simply bash, so we can compile what we need!
ENTRYPOINT ["/bin/bash"]